package ph.mahvine;

import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import ph.mahvine.century.UserVote;
import ph.mahvine.century.repositories.UserVoteRepository;

@SpringBootApplication
@Configuration
@ConfigurationProperties(prefix = "app")
@ComponentScan("ph.mahvine.century")
public class Application {

    public static void main(String[] args) {
        ApplicationContext context =SpringApplication.run(Application.class, args);
        UserVoteRepository userVoteRepository = context.getBean(UserVoteRepository.class);
        System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
        
    }
}
