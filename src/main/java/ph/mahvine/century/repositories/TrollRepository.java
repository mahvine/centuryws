package ph.mahvine.century.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ph.mahvine.century.Troll;

@Repository
public interface TrollRepository extends JpaRepository<Troll,Long>,JpaSpecificationExecutor<Troll>{

	public Page<Troll> findByIdLessThan(Long id, Pageable pageRequest);
	
	public Page<Troll> findByIdLessThanAndUserVoteCountry(Long id, String country, Pageable pageRequest);
	
}
