package ph.mahvine.century.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.mahvine.century.UserVote;

@Repository
public interface UserVoteRepository extends JpaRepository<UserVote,Long>{

	public UserVote findByUuid(String uuid);
	
	
	@Query(value="select boxer,count(boxer) from UserVote group by boxer")
	public List<Object[]> countUserVotes();
	

	@Query(value="select country, boxer,count(boxer) from UserVote group by boxer,country")
	public List<Object[]> countUserVotesByCountry();
	
	public List<UserVote> findByUuidStartingWith(String prefixUuid);
	
}
