package ph.mahvine.century;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_vote")
public class UserVote {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@Column(unique=true)
	public String uuid;
	
	public String boxer;
	
	public String country;
	
	public String name;
	
}
