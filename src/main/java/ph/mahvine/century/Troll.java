package ph.mahvine.century;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="troll")
@JsonInclude(Include.NON_NULL)
public class Troll {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String message;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(columnDefinition="user_vote_id",referencedColumnName="id")
	public UserVote userVote;
	
	@Column(name="date_created")
	public Date dateCreated;
	
	@Transient
	public String uuid;
	
	@Transient
	public String boxer;

	@Transient
	public String country;

	@Transient
	public String name;
	
	@PostLoad
	public void transform(){
		this.boxer = userVote.boxer;
		this.country = userVote.country;
		this.name = userVote.name;
	}
}
