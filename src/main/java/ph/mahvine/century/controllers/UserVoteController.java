package ph.mahvine.century.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ph.mahvine.century.UserVote;
import ph.mahvine.century.repositories.UserVoteRepository;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@RestController
@RequestMapping(value="/api/userVotes")
public class UserVoteController {

	@Autowired
	UserVoteRepository userVoteRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.CREATED)
	public void save(@RequestBody UserVote userVote, HttpServletResponse response) throws IOException{

		if(userVote.uuid==null||userVote.uuid.isEmpty()||userVote.country==null||userVote.country.isEmpty()||userVote.boxer==null||userVote.boxer.isEmpty()){
			response.sendError(400, "Required fields");
			return;
		}
		
		if( !(userVote.boxer.equalsIgnoreCase("m") || userVote.boxer.equalsIgnoreCase("p") )){
			response.sendError(400, "Invalid boxer");
			return;
		}
		
		
		
		
		UserVote userVote1 = userVoteRepository.findByUuid(userVote.uuid);
		if(userVote1 == null){
			userVote.boxer = userVote.boxer.toUpperCase();
			userVote.country = userVote.country.toUpperCase();
			userVoteRepository.save(userVote);
			if(userVote.boxer.equals("P")){
				userVote.name = "Pacquiao Supporter "+userVote.id;
			}
			if(userVote.boxer.equals("M")){
				userVote.name = "Mayweather Supporter "+userVote.id;
			}
			
			userVoteRepository.save(userVote);

		}else{
			response.sendError(400, "Already registered");
			return;
		}
	}
	

	@RequestMapping(method=RequestMethod.PUT)
	@ResponseStatus(value=HttpStatus.OK)
	public void updateName(@RequestBody UserVote userVote, HttpServletResponse response) throws IOException{

		if(userVote.uuid==null||userVote.uuid.isEmpty()||userVote.name==null||userVote.name.isEmpty()){
			response.sendError(400, "Required fields");
			return;
		}
		
		
		UserVote userVote1 = userVoteRepository.findByUuid(userVote.uuid);
		if(userVote1 != null){
			userVote1.name = userVote.name;
			userVoteRepository.save(userVote1);
		}else{
			response.sendError(400, "Please register");
			return;
		}
	}
	

	@RequestMapping(method=RequestMethod.GET,value="/{uuid}")
	public UserVote get(@PathVariable("uuid") String uuid, HttpServletResponse response) throws IOException{
		UserVote userVote = userVoteRepository.findByUuid(uuid);
		if(userVote!=null){
			return userVote;
		}else{
			response.sendError(404, "Invalid user.");
			return null;
		}
		
	}
	
	


	@RequestMapping(method=RequestMethod.GET,value="/count")
	public Object count(@RequestParam(value="group",required=false) String group, HttpServletResponse response) throws IOException{
		Map<String, UserVoteCount> aggregateCountry = new HashMap<String,UserVoteCount>(); 
		if(group!=null){
			if(group.equalsIgnoreCase("country")){
				List<Object[]> countryCountResultSet = userVoteRepository.countUserVotesByCountry();
				for(Object[] result:countryCountResultSet){
					String country = null;
					if (result[0] != null){
						country = (String) result[0];
					}
					UserVoteCount userVoteCount = aggregateCountry.get(country);
					if(userVoteCount==null){
						userVoteCount = new UserVoteCount();
						userVoteCount.country = country;
					}
					
					String boxer = null;
					if (result[1] != null){
						boxer = (String) result[1];
					}
					
					long count = 0;
					if (result[2] != null){
						count = (Long) result[2];
					}
					

					if(boxer.equalsIgnoreCase("M")){
						userVoteCount.M = count;
					}
					if(boxer.equalsIgnoreCase("P")){
						userVoteCount.P = count;
					}
					aggregateCountry.put(country, userVoteCount);
				}
				return aggregateCountry.values();
			}
		}
		
		List<Object[]> countResultSet = userVoteRepository.countUserVotes();
		UserVoteCount userVoteCountGlobal = new UserVoteCount();
		userVoteCountGlobal.date = new Date();
		for(Object[] result:countResultSet){
			String boxer = null;
			if (result[0] != null){
				boxer = (String) result[0];
			}
			long count = 0;
			if (result[1] != null){
				count = (Long) result[1];
			}
			if(boxer.equalsIgnoreCase("M")){
				userVoteCountGlobal.M = count;
			}
			if(boxer.equalsIgnoreCase("P")){
				userVoteCountGlobal.P = count;
			}
		}
		return userVoteCountGlobal;
	}
	
	
	
	@JsonInclude(Include.NON_NULL)
	public static class UserVoteCount{
		public long M;
		public long P;
		public Date date;
		public String country;
		
//		
//		public UserVoteCount(Map<String,Long> values){
//			for(Entry<String,Long> value: values.entrySet()){
//				
//				if(value.getKey().equalsIgnoreCase("M")){
//					this.M = value.getValue();
//				}
//				
//				if(value.getKey().equalsIgnoreCase("P")){
//					this.P = value.getValue();
//				}
//				
//				this.date = new Date();
//			}
//		}
		
	}
	
	
	@RequestMapping(method=RequestMethod.GET,value="/create")
	public String create(@RequestParam(value="prefix",required=true) String prefix,
			@RequestParam(value="num",required=true) int size,
			HttpServletResponse response) throws IOException{
		Random random = new Random();
      for(int i=0;i<size;i++){
      	long time = System.currentTimeMillis();
      	String boxer = (time %2 ==0) ? "M":"P";
      	int n = random.nextInt(3);
      	String country = "PH";
      	if(n==0){
      		country = "SG";
      	}else if(n==1){
      		country = "US";
      	}else if(n==2){
      		country = "JP";
      	}else if(n==3){
      		country = "BZ";
      	}
      	UserVote userVote = new UserVote();
      	userVote.boxer = boxer;
      	userVote.country = country;
      	userVote.uuid = prefix + time;
      	try {
      		userVoteRepository.save(userVote);
				Thread.sleep(n);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
      }
		
		
		return "done";
	}
	

	@RequestMapping(method=RequestMethod.GET,value="/delete")
	public String delete(@RequestParam(value="prefix",required=true) String prefix,
			HttpServletResponse response) throws IOException{
		List<UserVote> userVotes = userVoteRepository.findByUuidStartingWith(prefix);
		userVoteRepository.delete(userVotes);
		return "done";
	}
	
	
}
