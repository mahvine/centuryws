package ph.mahvine.century.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ph.mahvine.century.Troll;
import ph.mahvine.century.UserVote;
import ph.mahvine.century.repositories.TrollRepository;
import ph.mahvine.century.repositories.UserVoteRepository;

@RestController
@RequestMapping(value="/api/trolls")
public class TrollController {

	@Autowired
	UserVoteRepository userVoteRepository;

	@Autowired
	TrollRepository trollRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.CREATED)
	public void save(@RequestBody Troll troll){
		UserVote userVote = userVoteRepository.findByUuid(troll.uuid);
		if(troll.message != null){
			if(!troll.message.isEmpty()){
				if(userVote!=null){
					troll.dateCreated = new Date();
					troll.userVote = userVote;
					trollRepository.save(troll);
				}else{
					throw new RuntimeException("User not found:"+troll.uuid+" | Must register first.");
				}
			}
		}else{
			throw new RuntimeException("Required field message.");
		}
	}

	@RequestMapping(method=RequestMethod.GET)
	public List<Troll> list(@RequestParam(value="id",required=false) Long beforeId,
							@RequestParam(value="size",required=false,defaultValue="20") Integer size,
							@RequestParam(value="country",required=false) String country){
		PageRequest pageRequest = new PageRequest(0, size, Sort.Direction.DESC, "id");
		Page<Troll> pageTroll = trollRepository.findAll(filter(beforeId,country), pageRequest);
		return pageTroll.getContent();
	}
	
	public static Specification<Troll> filter(final Long beforeId, final String country) {
	        return new Specification<Troll>() {
				@Override
				public Predicate toPredicate(Root<Troll> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					final List<Predicate> predicates = new ArrayList<Predicate>();

					if(country!=null){
						if(!country.isEmpty()){
							predicates.add(criteriaBuilder.equal(root.<UserVote>get("userVote").<String>get("country"), country));
						}
					}

					if(beforeId != null){
						predicates.add(criteriaBuilder.lt(root.<Long>get("id"),beforeId));
					}
					
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
	        };
	    }
	
}
