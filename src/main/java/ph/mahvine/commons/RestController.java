package ph.mahvine.commons;


import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public abstract class RestController {

	protected int pageSize = 30;
	
	protected Gson gson;
	public RestController(){
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.setDateFormat("MM/dd/yyyy").registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
	    	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	    	DateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");
	        @Override
	        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
	                throws JsonParseException {
	            try {
	                return df.parse(json.getAsString());
	            } catch (ParseException e) {
	            	try {
		                return df1.parse(json.getAsString());
		            } catch (ParseException e1) {
		                return null;
		            }
	            }
	        }
	    });
	    gson = gsonBuilder.create();
	}
	
	protected Pageable pageRequest(int page, int pageSize, String sortByQuery, String directionQuery){
		String sortBy = "id";
		Direction direction = Direction.DESC;
		if ( sortByQuery != null) {
			if (!sortByQuery.isEmpty()) {
				sortBy = sortByQuery;
			}
		}
		
		if (directionQuery != null) {
			if (directionQuery.equalsIgnoreCase("ASC")) {
				direction = Direction.ASC;
			} else if (directionQuery.equalsIgnoreCase("DESC")) {
				direction = Direction.DESC;
			}
		}
		return new PageRequest(page, pageSize, new Sort( new Order(direction, sortBy)));
	}

}
